import { Component } from '@angular/core';
import {StudentService} from './student.service'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Services';
  rollno;
  name ="";
  standard;

  constructor(private student:StudentService){
    console.warn(this.student.getDetails())
    this.rollno = this.student.getDetails().rollno
    this.name = this.student.getDetails().name
    this.standard = this.student.getDetails().standard
  }
  
}
