import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  constructor() {}
  data:Observable<any> | undefined;
  ngOnInit(): void {
    this.data=new Observable(observer => {
      setTimeout(() => {
        observer.next(1);
      },1000);

      setTimeout(() => {
        observer.next(2);
      },4000);
      
      /*setTimeout(() => {
        observer.error();
      },5000);*/

      setTimeout(() => {
        observer.next(3);
      },6000);
      
      setTimeout(() => {
        observer.complete();
      },7000);
    });
    let subscriber1=this.data.subscribe(value =>{
        console.log("Subscriber 1",value) ;
       // subscriber1.unsubscribe();
    },error =>{
        console.log("error")
    });

    let subscriber2=this.data.subscribe(value =>{
      console.log("Subscriber 2",value) ;
  },error =>{
      console.log("error")
  });

  }

}

