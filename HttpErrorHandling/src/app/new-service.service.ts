import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import {catchError} from 'rxjs/operators';
import { throwError } from 'rxjs/internal/observable/throwError';

@Injectable({
  providedIn: 'root'
})
export class NewServiceService {

  constructor(private http: HttpClient) { }

  getData(): Observable<any>{
    const url="https://api.github.com/user"
    return this.http.get<any>(url)
                     .pipe(catchError(this.handleError))
  }

  handleError(error: { message: any; }){
    return throwError(error.message)
  }
}
