import { Component } from '@angular/core';
import {NewServiceService} from './new-service.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  error:String | undefined
  data:String | undefined
  constructor(private newService:NewServiceService){}
  dataGet(){
    this.newService.getData().subscribe((data) => {
      console.log(data)
    },(error) => {
      console.log(error)
      this.error=error
    })
  }
}
