import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'StructuralDirective';
  public displayName = false;
  public number = "two";
  public numbers = ["zero","one","two","three"]
}
