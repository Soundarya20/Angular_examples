import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'String Interpolation';

  image = 'https://www.naturespicsonline.com/system/carousel_image/file/193/0.jpg';

  goBack(){
    console.log("Page Not available");
  } 

  message: string = ""; 
}

