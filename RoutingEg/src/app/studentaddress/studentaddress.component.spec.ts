import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentaddressComponent } from './studentaddress.component';

describe('StudentaddressComponent', () => {
  let component: StudentaddressComponent;
  let fixture: ComponentFixture<StudentaddressComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudentaddressComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentaddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
