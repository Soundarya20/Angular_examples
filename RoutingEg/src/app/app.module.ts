import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule, routingComponent } from './app-routing.module';
import { AppComponent } from './app.component';
import { StudentdetailComponent } from './studentdetail/studentdetail.component';
import {StudentComponent} from './student/student.component';
import { StudentcontactComponent } from './studentcontact/studentcontact.component';
import { StudentaddressComponent } from './studentaddress/studentaddress.component';

@NgModule({
  declarations: [
    AppComponent,
    StudentdetailComponent,
    StudentComponent,
    StudentcontactComponent,
    StudentaddressComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
