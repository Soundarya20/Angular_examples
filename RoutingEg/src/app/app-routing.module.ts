import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StudentComponent } from './student/student.component';
import { StudentdetailComponent } from './studentdetail/studentdetail.component';
import { TeacherComponent } from './teacher/teacher.component';
import { StudentcontactComponent } from './studentcontact/studentcontact.component';
import { StudentaddressComponent } from './studentaddress/studentaddress.component';
const routes: Routes = [
  {path: 'student', component: StudentComponent},
  {path: 'teacher', component: TeacherComponent},
  {path: 'student/:rollno',component: StudentdetailComponent, 
   children :[
     {path:'contact', component:StudentcontactComponent},
     {path:'address',component:StudentaddressComponent}
   ] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponent = [StudentComponent,
   TeacherComponent,
   StudentdetailComponent,
   StudentcontactComponent,
   StudentaddressComponent
  ]