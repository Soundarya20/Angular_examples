import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router'
@Component({
  selector: 'app-student',
  template: `
    <h3> Student List</h3>
      <div (click)="onSelect(student)" *ngFor="let student of students">
        <span> {{student.rollno}}  </span>   {{student.name}}
      </div>
    
  `,
  styles: [
  ]
})
export class StudentComponent implements OnInit {
  constructor(private router : Router) { }
students = [
    {"rollno":101,"name":"Ashok"},
    {"rollno":102,"name":"Dharshini"},
    {"rollno":103,"name":"Harini"},
    {"rollno":104,"name":"Nivetha"}
  ]
  ngOnInit() {
  }
  onSelect(student: { rollno: any; }){
      this.router.navigate(['/student',student.rollno]);
      
  }
}
