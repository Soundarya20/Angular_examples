import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router} from '@angular/router';

@Component({
    template: `
    <p> Selected student with roll no : {{studrollno}} </p>
    <p>
    <button (click)="viewContact()">Contact </button><br/><br/>
    <button (click)="viewAddress()"> Address </button>
    </p>
    <router-outlet></router-outlet>
    
    `
})
export class StudentdetailComponent implements OnInit {
  
 
   public studrollno: any;
  constructor(private route : ActivatedRoute,private router:Router){}
  
  ngOnInit() {
   
    let rollno = this.route.snapshot.paramMap.get('rollno');
    this.studrollno=rollno;

  }
  viewContact(){
    this.router.navigate(['contact'],{relativeTo:this.route});
  }
  viewAddress(){
    this.router.navigate(['address'],{relativeTo:this.route});
  }
}
